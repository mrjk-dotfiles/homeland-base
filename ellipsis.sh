#!/usr/bin/env bash


pkg.link() {

    case $(os.platform) in
        cygwin)
            fs.link_files cygwin
            ;;
        osx)
            fs.link_files osx
            ;;
        freebsd)
            fs.link_files freebsd
            ;;
        linux)
            fs.link_files linux
            ;;
    esac

    #fs.link_files config .config 
    mkdir -p "$HOME/.config"
    for file in $(find "config" -maxdepth 1 ) ; do
      [ ! "config" = "$file" ] && fs.link_file "$file" "$HOME/.$file"
    done
}



